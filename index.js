function checkPrimeNum(num) {
  var flag = true;
  if (num <= 1) flag = false;
  for (var i = 2; i <= Math.sqrt(num); i++) {
    if (num % i == 0) flag = false;
  }
  return flag;
}

function inSoNguyenTo() {
  var num = document.getElementById("number").value * 1;
  var content = "";
  for (var i = 1; i <= num; i++) {
    if (checkPrimeNum(i) == true) content += i + " ";
  }
  document.getElementById(
    "ketQua"
  ).innerHTML = `Các số nguyên tố là: ${content}`;
  document.getElementById("ketQua").style.backgroundColor = "#d1e7dd";
  document.getElementById("ketQua").style.padding = "10px";
  document.getElementById("ketQua").style.marginTop = "10px";
}
